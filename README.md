# Configuration

## Install stow
This repository is configured to work with GNU stow.
To use stow, install it using your distro's package manager.

### Fedora:
    
    # dnf -y install stow

## Before Continuing

There's a folder in the root of this repository for each application
where dotfiles are provided.
Before continuing, you'll need to backup your existing dotfiles for
any of the application's configurations you wish to replace with the
ones provided in this repository.

## Examples:

These examples assume you cloned this repository in your home folder.
Either move it there if that's not the case or consult stow's manual
and change them as required for your use case.

The following command will link the dotfiles in `<repo folder>` to where they
should be in your home folder.
    
    $ stow <repo folder>

For NeoVim, the command would be
    
    $ stow nvim

# Dependencies
## Zsh
* [Oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
* [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)

## Kitty
* [JetBrains Mono font](https://www.jetbrains.com/lp/mono/)

## Neovim
The following language servers are configured in this configuration:
* clangd - C/C++
* pyls - Python
* html-languageserver - HTML/CSS/Javascript
