" vim-plug plugin manager
call plug#begin('~/.config/nvim/plugged')
Plug 'scrooloose/nerdcommenter'
Plug 'mphe/grayout.vim'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-obsession'
Plug 'jiangmiao/auto-pairs'
Plug 'majutsushi/tagbar'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'lervag/vimtex', {'for': 'latex'}
Plug 'vhdirk/vim-cmake'
Plug 'edkolev/tmuxline.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'

" Intellisense (LSP)
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'yami-beta/asyncomplete-omni.vim'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'

" Colorschemes
Plug 'NLKNguyen/papercolor-theme'
Plug 'arcticicestudio/nord-vim'
call plug#end()

set number
set hlsearch
set mouse=a
set switchbuf=split
set splitbelow
set splitright
set scrolloff=5
set wildignorecase
set smartcase
set hidden
set linebreak
syntax enable
let mapleader = " "

" code formatting
set autoindent
autocmd FileType c setlocal shiftwidth=4 tabstop=4 cindent
autocmd FileType java setlocal shiftwidth=4 tabstop=4
autocmd FileType cpp setlocal shiftwidth=4 tabstop=4 cindent
autocmd FileType sh setlocal shiftwidth=2 tabstop=2
autocmd FileType python setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType text setlocal shiftwidth=4 tabstop=4 spell spelllang=en_us,el
let g:tex_flavor = "latex"
autocmd FileType tex setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType tex setlocal spell spelllang=en_us,el
"autocmd FileType tex setlocal tw=80 fo=a2wt
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType cmake setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType make setlocal shiftwidth=2 tabstop=2
autocmd FileType markdown setlocal tw=80 fo=a2wt spell spelllang=en_US,el
autocmd BufNewFile,BufRead *.C4 setlocal filetype=c
autocmd TermOpen * setlocal nonumber norelativenumber
autocmd FileType html,css,javascript setlocal shiftwidth=2 tabstop=2 expandtab smartindent

" underline spelling errors
highlight clear SpellBad
highlight SpellBad cterm=underline

" Custom key bindings
" <Space> toggle existing folding or fold selected lines
nnoremap <silent> <Leader><Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Leader><Space> zf

" Keyboard mappings
" Toggle numbering
nnoremap <silent> <F2> :exe 'set nu!' &nu ? 'rnu!' : ''<CR>
" Escape to normal mode from insert and terminal mode using jj
inoremap jj <ESC>
tnoremap JJ <C-\><C-n>
"" Map Tab and Shift+Tab to cycle completions
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Custom commands
command VimConfigEdit :e ~/.config/nvim/init.vim
command VimConfigSource :source ~/.config/nvim/init.vim

" Plugin settings

" vim-airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#tabs_label = ''
let g:airline#extensions#tabline#show_tab_count = 0
let g:airline#extensions#tmuxline#enabled = 0
let g:airline_left_sep = ' '
let g:airline_left_alt_sep = '|'
let g:airline_right_sep = ' '
let g:airline_right_alt_sep = '|'
let g:airline_powerline_fonts = 1
let g:airline_exclude_preview = 1
let g:airline_section_warning = ''
set noshowmode " airline already includes the mode in its status line

" tmuxline
let g:tmuxline_theme = "vim_statusline_2"
let g:tmuxline_preset = "minimal"

" tagbar
let g:tagbar_sort = 0
nnoremap <Leader>t :TagbarToggle<CR>

" fugitive
" Mnemonic: g[s]_ -> Git [split] command-first-letter
nnoremap <Leader>gs :vert Git<CR>
nnoremap <Leader>gss :Git<CR>
nnoremap <Leader>gl :vert Git log<CR>
nnoremap <Leader>gsl :Git log<CR>
nnoremap <Leader>gd :Gdiff<CR>

" vim-lsp
let g:lsp_signs_enabled = 1
let g:lsp_diagnostics_echo_cursor = 1
let g:lsp_virtual_text_enabled = 0
let g:lsp_signs_error = {'text': '✗'}
let g:lsp_signs_warning = {'text': '‼'}
"let g:lsp_log_verbose = 1
"let g:lsp_log_file = 'vim-lsp.log'
" Register clangd language server
if executable('clangd')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'clangd',
        \ 'cmd': {server_info->['clangd', '--background-index', '--clang-tidy', '--header-insertion=never']},
        \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
    \ })
    autocmd FileType c,cpp call LspMappings()
endif
" Register Python language server
if executable('pyls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'pyls',
        \ 'cmd': {server_info->['pyls']},
        \ 'whitelist': ['python'],
    \ })
    autocmd FileType python call LspMappings()
endif
" HTMl/CSS/JavaScript language server
if executable('html-languageserver')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'html-languageserver',
        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'html-languageserver --stdio']},
        \ 'whitelist': ['html', 'css', 'javascript', 'typescript'],
    \ })
endif

" Initialize my key-mappings for vim-lsp
function LspMappings()
    " Use language server for navigation to definition
    nmap <buffer> <silent> <C-]> :LspDefinition<CR>
    nmap <buffer> <silent> <C-w><C-]> :rightbelow LspDefinition<CR>

    nnoremap <buffer> <Leader>lh :LspHover<CR>
    nnoremap <buffer> <Leader>lr :LspReferences<CR>
    nnoremap <buffer> <Leader>lsl :LspDocumentSymbol<CR>
    nnoremap <buffer> <Leader>lss :LspWorkspaceSymbol<CR>
    nnoremap <buffer> <Leader>ldd :LspDocumentDiagnostics<CR>
endfunction

" Add omnicompletion
call asyncomplete#register_source(asyncomplete#sources#omni#get_source_options({
\ 'name': 'omni',
\ 'whitelist': ['*'],
\ 'blacklist': ['c', 'cpp', 'html'],
\ 'completor': function('asyncomplete#sources#omni#completor')
\  }))

" Mappings for fzf.vim
silent! !git rev-parse --is-inside-work-tree
if v:shell_error == 0
	nnoremap <Leader>e :GFiles<CR>
else
	nnoremap <Leader>e :Files<CR>
endif
nnoremap <Leader>b :Buffers<CR>

" vim-gitgutter colors
let g:gitgutter_preview_win_floating = 0 " Use split instead of float to preview hunks

" fzf.vim colors
function! s:update_fzf_colors()
  let rules =
  \ { 'fg':      [['Normal',       'fg']],
    \ 'bg':      [['Normal',       'bg']],
    \ 'hl':      [['Comment',      'fg']],
    \ 'fg+':     [['CursorColumn', 'fg'], ['Normal', 'fg']],
    \ 'bg+':     [['CursorColumn', 'bg']],
    \ 'hl+':     [['Statement',    'fg']],
    \ 'info':    [['PreProc',      'fg']],
    \ 'prompt':  [['Conditional',  'fg']],
    \ 'pointer': [['Exception',    'fg']],
    \ 'marker':  [['Keyword',      'fg']],
    \ 'spinner': [['Label',        'fg']],
    \ 'header':  [['Comment',      'fg']] }
  let cols = []
  for [name, pairs] in items(rules)
    for pair in pairs
      let code = synIDattr(synIDtrans(hlID(pair[0])), pair[1])
      if !empty(name) && code > 0
        call add(cols, name.':'.code)
        break
      endif
    endfor
  endfor
  let s:orig_fzf_default_opts = get(s:, 'orig_fzf_default_opts', $FZF_DEFAULT_OPTS)
  let $FZF_DEFAULT_OPTS = s:orig_fzf_default_opts .
        \ empty(cols) ? '' : (' --color='.join(cols, ','))
endfunction

augroup _fzf
  autocmd!
  autocmd ColorScheme * call <sid>update_fzf_colors()
augroup END

" Solarized dark/light color settings
function SolarizedDark()
	colorscheme solarized
        set background=dark
        " vim-gitgutter colors
        highlight SignColumn ctermbg=8
        highlight GitGutterAdd ctermbg=8 ctermfg=2
        highlight GitGutterDelete ctermbg=8 ctermfg=1
        highlight GitGutterChange ctermbg=8 ctermfg=9
        "vim-airline
        let g:airline_solarized_bg = 'dark'
        let g:airline_theme = 'solarized'
	" Search highlight
	highlight clear Search
	highlight Search cterm=bold ctermfg=13

endfunction

function SolarizedLight()
        colorscheme solarized
        set background=light
        " vim-gitgutter colors
        highlight SignColumn ctermbg=7
        highlight GitGutterAdd ctermbg=7 ctermfg=2
        highlight GitGutterDelete ctermbg=7 ctermfg=1
        highlight GitGutterChange ctermbg=7 ctermfg=9
        "vim-airline
        let g:airline_solarized_bg = 'light'
        let g:airline_theme = 'solarized'
	" Search highlight
	highlight clear Search
	highlight Search cterm=bold ctermfg=13
endfunction

"set termguicolors
colorscheme nord
